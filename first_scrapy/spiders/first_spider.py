from pathlib import Path
from typing import Any
import scrapy
from scrapy.http import Response

from first_scrapy.items import DoctorlistItem, QuotesItem
import pandas as pd

class TestSpider(scrapy.Spider):
    name = "test_spider"
    # allowed_domains= ["toscrape.com"]
    # start_urls = [
    #         "https://quotes.toscrape.com/page/1/",
    #         "https://quotes.toscrape.com/page/2/",
    #     ]
    start_urls = ["https://quotes.toscrape.com/"]
    QUOTE_SELECTOR = '.quote'
    TEXT_SELECTOR = '.text::text'
    AUTHOR_SELECTOR = '.author::text'
    ABOUT_SELECTOR = '.author + a::attr("href")'
    TAGS_SELECTOR = '.tags > .tag::text'
    NEXT_SELECTOR = '.next a::attr("href")'

    def parse(self, response: Response, **kwargs: Any) -> Any:
        for quote in response.css(self.QUOTE_SELECTOR): 
            yield QuotesItem(
                text= quote.css(self.TEXT_SELECTOR).extract_first(),
                author= quote.css(self.AUTHOR_SELECTOR).extract_first(),
                about= 'https://quotes.toscrape.com' + 
                        quote.css(self.ABOUT_SELECTOR).extract_first(),
                tags= quote.css(self.TAGS_SELECTOR).extract(), 
            )

            next_page = response.css(self.NEXT_SELECTOR).extract_first()
            if next_page:
                # yield scrapy.Request(response.urljoin(next_page))
                yield response.follow(next_page, callback=self.parse)

    # def start_requests(self):
    #     urls = [
    #         "https://quotes.toscrape.com/page/1/",
    #         "https://quotes.toscrape.com/page/2/",
    #     ]
    #     for url in urls:
    #         yield scrapy.Request(url=url, callback=self.parse)

    # def parse(self, response):
    #     page = response.url.split("/")[-2]
    #     filename = f"quotes-{page}.html"
    #     Path(filename).write_bytes(response.body)
    #     self.log(f"Saved file {filename}")  
                


class DoctorSpider(scrapy.Spider): 
    name = 'doctor'
    allowed_domains = ['practo.com'] 
    start_urls = ["https://www.practo.com/thiruvananthapuram/general-physician",
                # "https://www.practo.com/kollam/general-physician",
                # "https://www.practo.com/pathanamthitta/general-physician",
                # "https://www.practo.com/alappuzha/general-physician",
                # "https://www.practo.com/kottayam/general-physician",
                # "https://www.practo.com/idukki/general-physician",
                # "https://www.practo.com/ernakulam/general-physician",
                # "https://www.practo.com/thrissur/general-physician",
                # "https://www.practo.com/palakkad/general-physician",
                # "https://www.practo.com/malappuram/general-physician",
                # "https://www.practo.com/kozhikode/general-physician",
                # "https://www.practo.com/wayanad/general-physician",
                # "https://www.practo.com/kannur/general-physician",
                # "https://www.practo.com/kasaragod/general-physician"
                ]
    HEADERS = {
        'User-Agent': 'rogerbot'
    }

    NEXT_SELECTOR = ".c-paginator span[data-qa-id='pagination_next'] a::attr('href')"
    df = pd.DataFrame()

        
    def parse(self, response):    
       
        for section in response.css('info-section'):    
            doctor_dict = DoctorlistItem( 
                doctor_name=section.css("h2 > .doctor-name::text").extract_first(), 
                experience=section.css(".uv2-spacer--xs-top::text").extract_first(), 
                specialization=section.css(".u-d-flex > span::text").extract_first(), 
                locality = (section.css("div.u-bold.u-d-inlineblock.u-valign--middle")).css("span[data-qa-id='practice_locality']::text").extract_first(),
                city = (section.css("div.u-bold.u-d-inlineblock.u-valign--middle")).css("span.u-t-capitalize[data-qa-id='practice_city']::text").extract_first(),
                clinic_name = section.css("div.u-d-inlineblock.u-valign--middle> span.u-t-hover-underline ::text").extract_first(),
                consultation_fee = section.css("span[data-qa-id='consultation_fee']::text").extract_first()                
            )  
            df_dictionary = pd.DataFrame([doctor_dict])
            self.df = pd.concat([self.df, df_dictionary], ignore_index=True)  
            yield  doctor_dict

            next_page = response.css(self.NEXT_SELECTOR).extract_first()
            if next_page:
                # yield scrapy.Request(response.urljoin(next_page))
                # yield response.follow(next_page, callback=self.parse)
                yield scrapy.Request(url=next_page, callback=self.parse, headers=self.HEADERS, cookies=response.request.cookies)

            else:
                excel_file_name = "doctor_details.xlsx"
                self.df.to_excel(excel_file_name, index=False) 
