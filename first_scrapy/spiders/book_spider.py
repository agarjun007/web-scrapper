from first_scrapy.items import BooklistItem
import scrapy

from scrapy.http import Response

import pandas as pd


class BookSpider(scrapy.Spider): 
    name = 'book'
    allowed_domains = ['books.toscrape.com'] 
    start_urls = ['http://books.toscrape.com/'] 
    NEXT_SELECTOR = '.next a::attr("href")'
    df = pd.DataFrame()
  
    def parse(self, response):    
        for article in response.css('article.product_pod'):    
            book_dict = BooklistItem( 
                url=article.css("h3 > a::attr(href)").get(), 
                title=article.css("h3 > a::attr(title)").extract_first(), 
                price=article.css(".price_color::text").extract_first(), 
            )  
            df_dictionary = pd.DataFrame([book_dict])
            self.df = pd.concat([self.df, df_dictionary], ignore_index=True)  
            yield  book_dict
            next_page = response.css(self.NEXT_SELECTOR).extract_first()
            if next_page:
                # yield scrapy.Request(response.urljoin(next_page))
                yield response.follow(next_page, callback=self.parse)
            else:
                excel_file_name = "book_details.xlsx"
                self.df.to_excel(excel_file_name, index=False) 
