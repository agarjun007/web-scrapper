# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class QuotesItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    text = scrapy.Field()    
    author = scrapy.Field()    
    about = scrapy.Field()  
    tags  = scrapy.Field()

class BooklistItem(scrapy.Item): 
    url = scrapy.Field() 
    title =scrapy.Field() 
    price = scrapy.Field() 

class DoctorlistItem(scrapy.Item): 
    doctor_name = scrapy.Field() 
    experience =scrapy.Field() 
    specialization = scrapy.Field()   
    locality = scrapy.Field()    
    city = scrapy.Field()    
    clinic_name = scrapy.Field()    
    consultation_fee = scrapy.Field()